<h1 align="center">Welcome to Genelle's Music 👋</h1>
<p>
</p>

> Code for Genelle's music website.

## Author

👤 **Jason D.**

* Website: eyedarts.com
* Github: [@ewc](https://github.com/ewc)

## Show your support

Give a ⭐️ if this project helped you!

***
_This README was generated with ❤️ by [readme-md-generator](https://github.com/kefranabg/readme-md-generator)_