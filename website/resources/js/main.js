window.onload = function() {
  // Get button element and elements with nav-link class
  let menuButton = document.getElementById("menu-button");
  let navMenu = document.getElementById("menu-items");

  menuButton.onclick = function() {
    // If navMenu is hidden, display it. Else, hide it.
    console.log("clicked");
    navMenu.classList.toggle("menu-toggled");
  };
};
